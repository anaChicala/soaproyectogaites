package ar.edu.iua.proyectoSOA.model.persistance;

import ar.edu.iua.proyectoSOA.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Employee, Integer> {

    Employee findByLegajo(String legajo);
    List<Employee> findAll();

}
