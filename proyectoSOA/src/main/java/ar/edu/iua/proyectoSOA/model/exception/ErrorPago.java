package ar.edu.iua.proyectoSOA.model.exception;

public class ErrorPago extends Exception {

    @Override
    public String getMessage() {
        return "El pago devolvio un estado de ERROR";
    }
}
