package ar.edu.iua.proyectoSOA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoSoaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoSoaApplication.class, args);
	}

}
