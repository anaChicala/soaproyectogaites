package ar.edu.iua.proyectoSOA.business;

import ar.edu.iua.proyectoSOA.model.TransactionDto;
import ar.edu.iua.proyectoSOA.model.PaymentStatusDto;
import ar.edu.iua.proyectoSOA.model.TransactionDtoGet;
import ar.edu.iua.proyectoSOA.model.exception.BadRequest;
import ar.edu.iua.proyectoSOA.model.exception.ErrorPago;
import ar.edu.iua.proyectoSOA.model.exception.NotFound;

import java.util.Date;
import java.util.List;

public interface TransactionBusinessI {

    PaymentStatusDto generatePayment(TransactionDto transactionDto) throws BadRequest, NotFound, ErrorPago;
    List<TransactionDtoGet> getPaymentByDate(Date date) throws BadRequest, NotFound;
}