package ar.edu.iua.proyectoSOA.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Transaccion")

public class Transaccion {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int paymentId;
    private String legajo;
    private double monto;
    private Date fecha;
    private String estado;
    private String codigo;

    public Transaccion(String legajo, double monto, Date fecha, String estado, String codigo) {
        this.legajo = legajo;
        this.monto = monto;
        this.fecha = fecha;
        this.estado = estado;
        this.codigo = codigo;
    }

    public Transaccion() {
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}