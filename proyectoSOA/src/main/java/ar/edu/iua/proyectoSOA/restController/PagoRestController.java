package ar.edu.iua.proyectoSOA.restController;

import ar.edu.iua.proyectoSOA.business.TransactionBusinessI;
import ar.edu.iua.proyectoSOA.model.TransactionDto;
import ar.edu.iua.proyectoSOA.model.PaymentStatusDto;
import ar.edu.iua.proyectoSOA.model.TransactionDtoGet;
import ar.edu.iua.proyectoSOA.model.exception.BadRequest;
import ar.edu.iua.proyectoSOA.model.exception.ErrorPago;
import ar.edu.iua.proyectoSOA.model.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(Constant.TRANSACTION_URL)
public class PagoRestController {
    @Autowired
    private TransactionBusinessI pagoBusiness;

    @GetMapping(value = {"verPagos"})
    public ResponseEntity<List<TransactionDtoGet>> Payment(@RequestParam( value = "fecha") String fecha){
        try{

            Date date1=new SimpleDateFormat("dd/MM/yy").parse(fecha);
            return new ResponseEntity<List<TransactionDtoGet>>(pagoBusiness.getPaymentByDate(date1), HttpStatus.OK);

        }catch(BadRequest e){

            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch  (NotFound e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = {"pagar","pagar/"})
    public ResponseEntity<PaymentStatusDto> generatePayment(@RequestBody TransactionDto transactionDto){
        try {
            PaymentStatusDto paymentStatusDto = pagoBusiness.generatePayment(transactionDto);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("location", "");
            return new ResponseEntity<>(paymentStatusDto, responseHeaders, HttpStatus.CREATED);
        } catch(BadRequest e){
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch  (NotFound e){
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch  (ErrorPago e){
            System.out.println(e.getMessage() + " in legajo: " + transactionDto.getLegajo() );
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }catch  (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
