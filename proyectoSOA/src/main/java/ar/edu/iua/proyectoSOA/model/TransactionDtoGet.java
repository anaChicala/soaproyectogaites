package ar.edu.iua.proyectoSOA.model;

import java.util.Date;

public class TransactionDtoGet {
    private String legajo;
    private double monto;
    private String estadoTransaccion;
    private String codigoTransaccion;
    private String fechaDePago;

    public TransactionDtoGet(String legajo, double monto, String estadoTransaccion, String codigoTransaccion, String fechaDePago) {
        this.legajo = legajo;
        this.monto = monto;
        this.estadoTransaccion = estadoTransaccion;
        this.codigoTransaccion = codigoTransaccion;
        this.fechaDePago = fechaDePago;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getEstadoTransaccion() {
        return estadoTransaccion;
    }

    public void setEstadoTransaccion(String estadoTransaccion) {
        this.estadoTransaccion = estadoTransaccion;
    }

    public String getCodigoTransaccion() {
        return codigoTransaccion;
    }

    public void setCodigoTransaccion(String codigoTransaccion) {
        this.codigoTransaccion = codigoTransaccion;
    }

    public String getFechaDePago() {
        return fechaDePago;
    }

    public void setFechaDePago(String fechaDePago) {
        this.fechaDePago = fechaDePago;
    }
}
