package ar.edu.iua.proyectoSOA.business.implementation;


import ar.edu.iua.proyectoSOA.business.TransactionBusinessI;
import ar.edu.iua.proyectoSOA.model.*;
import ar.edu.iua.proyectoSOA.model.exception.BadRequest;
import ar.edu.iua.proyectoSOA.model.exception.ErrorPago;
import ar.edu.iua.proyectoSOA.model.exception.NotFound;
import ar.edu.iua.proyectoSOA.model.persistance.PersonRepository;
import ar.edu.iua.proyectoSOA.model.persistance.TransactionRepository;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

@Service
public class TransactionBusiness implements TransactionBusinessI {

    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    PersonRepository personRepository;


    @Override
    public PaymentStatusDto generatePayment(TransactionDto transactionDto) throws BadRequest, NotFound, ErrorPago {


        if(transactionDto.getLegajo() == null){
            throw new BadRequest();
        }

        Employee employee =  personRepository.findByLegajo(transactionDto.getLegajo());
        if(employee == null){
            throw new NotFound();
        }

        AuthorizationDto authorizationDto = new AuthorizationDto(employee.getCbu(), employee.getMontoMensual());
        Gson gson = new Gson();
        String json = gson.toJson(authorizationDto);
        HttpClient httpClient =  HttpClientBuilder.create().build();
        String responseString = "";
        try{
            HttpPost request = new HttpPost("https://iua-service.herokuapp.com/transferir");
            StringEntity params = new StringEntity(json);
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            responseString = new BasicResponseHandler().handleResponse(response);
            //System.out.println(responseString);
            //if(responseStringestado !=)
        }catch (Exception e){
            System.out.println(e);
        }finally {

        }
        AuthorizationAnswerDto authorizationAnswerDto = new Gson().fromJson(responseString, AuthorizationAnswerDto.class);
        String cardReturnStatus = authorizationAnswerDto.getEstado();

        String cardReturnApprovalCode = authorizationAnswerDto.getCodigo();

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");

        String dateString = format.format(new Date());
        Date date = null;
        try {
            date = format.parse( dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Transaccion transaccion = new Transaccion(employee.getLegajo(), employee.getMontoMensual(), date, cardReturnStatus, cardReturnApprovalCode);
        if(transaccion.getEstado().equals("ERROR")){
            throw new ErrorPago();
        }
        Transaccion returnedTransaccion = transactionRepository.save(transaccion);


        return new PaymentStatusDto(returnedTransaccion.getPaymentId(), returnedTransaccion.getCodigo(), returnedTransaccion.getEstado());
    }

    public  List<TransactionDtoGet> getPaymentByDate (Date date) throws BadRequest, NotFound {



        if(date == null){
            throw new BadRequest();
        }

        System.out.println("Date"+ date);
        List<Transaccion> transacciones = transactionRepository.findByFecha(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
        List<TransactionDtoGet> transaccionesDto = new ArrayList<TransactionDtoGet>();
        System.out.println("Transacciones"+ transacciones.size());

        if(transacciones.isEmpty()){
            throw new NotFound();
        }

        for (Transaccion t: transacciones) {
            String fecha =simpleDateFormat.format(t.getFecha());
            TransactionDtoGet transaccionDto = new TransactionDtoGet(t.getLegajo(),t.getMonto(),t.getEstado(),t.getCodigo(),fecha);
            transaccionesDto.add(transaccionDto);
        }
        return  transaccionesDto;
    }
}
