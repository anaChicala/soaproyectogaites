package ar.edu.iua.proyectoSOA.model;

import javax.persistence.*;

@Entity
@Table(name = "Persona")
public class Employee {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idPersona;
    private String legajo;
    private String cbu;
    private double montoMensual;

    public Employee() {

    }

    public Employee(String cbu, double montoMensual, String legajo) {
        this.idPersona = idPersona;
        this.cbu = cbu;
        this.montoMensual = montoMensual;
        this.legajo = legajo;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }
    public double getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(double montoMensual) {
        this.montoMensual = montoMensual;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }
}
