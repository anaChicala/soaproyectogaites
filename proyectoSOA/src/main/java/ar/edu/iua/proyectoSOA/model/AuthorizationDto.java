package ar.edu.iua.proyectoSOA.model;

public class AuthorizationDto {

    private String cbu;
    private Double monto;

    public AuthorizationDto(String cbu, Double monto) {
        this.cbu = cbu;
        this.monto = monto;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }
}