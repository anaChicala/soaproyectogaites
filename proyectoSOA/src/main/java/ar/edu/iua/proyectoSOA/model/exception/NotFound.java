package ar.edu.iua.proyectoSOA.model.exception;

public class NotFound extends Exception{

    @Override
    public String getMessage() {
        return "No se ha encontrado el elemento solicitado";
    }
}
