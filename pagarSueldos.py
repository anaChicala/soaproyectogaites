#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pagarSueldos.py
#  
#  Copyright 2019 Rodrigo <rviano662@alumnos.iua.edu.ar>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import MySQLdb as mysql
import requests

class BaseDeDatos():
    def connectDatabase(self, usr, passwrd, database, url):
        return mysql.connect(url, usr, passwrd, database)

    def selectLegajos(self, dbcursor):
        legajos = []
        dbcursor.execute("SELECT legajo FROM persona")
        for item in dbcursor:
            legajos.append(item)
            
        return legajos
        
    def callApi(self, legajos):
        url = 'http://localhost:8080/api/v1/pagar'
        headers = {'Content-Type' : 'application/json'}
        for legajo in legajos:
            cuerpo = {'legajo': str(legajo[0])}
            response = requests.post(url, headers = headers, json=cuerpo)
            
            if response.status_code == 201:
                print ("Pago realizado para el legajo: %s" %(legajo[0]))
            if response.status_code == 500:
                print ("Error al realizar el pago para : %s" %(legajo[0]))
            if response.status_code == 404:
                print ("El legajo %s no existe" %(legajo[0]))
            if response.status_code == 400:
                print ("Error al hacer el request")
        
def main(args):
    user = 'root'
    password = 'root'
    url = 'localhost'
    db= 'Transactions'
    bdd = BaseDeDatos()
    connection = bdd.connectDatabase(user, password, db, url)
    cursor = connection.cursor()
    legajos = bdd.selectLegajos(cursor)
    bdd.callApi(legajos)
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
